/*!
 * \author Ahmad Yoosofan <yoosofan@mail.com> <yoosofan@myfastmail.com>
 * \version 0.0.1
 * \date 2018
 * \copyright View only (not copy, not use, not even compile) just for the course in the university of Kashan by Ahmad Yoosofan
*/

#include <stdio.h>
#include "complexCls.h"
#include "list_complexCls.h"
int main(){
	complexCls a={2,3},b={4,5};
	Node_complexCls head;
	head.next=0;
	insert_first(&head,a);
	insert_first(&head,b);
	print_list_complexCls(head);
	return 0;
}
