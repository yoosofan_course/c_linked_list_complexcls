/*!
 * \author Ahmad Yoosofan <yoosofan@mail.com> <yoosofan@myfastmail.com>
 * \version 0.0.1
 * \date 2018
 * \copyright View only (not copy, not use, not even compile) just for the course in the university of Kashan by Ahmad Yoosofan
*/

#include <stdio.h>
#include "complexCls.h"
void print_complexCls(complexCls a)
{printf("%lf+i%lf\n",a.re,a.img);}
complexCls input_complexCls(void){
	complexCls a;
	printf("Enter real part\t");
	scanf("%lf",&a.re);
	printf("Enter imaginary part\t");
	scanf("%lf",&a.img);
	return a;
}

