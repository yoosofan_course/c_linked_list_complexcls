/*!
 * \author Ahmad Yoosofan <yoosofan@mail.com> <yoosofan@myfastmail.com>
 * \version 0.0.1
 * \date 2018
 * \copyright View only (not copy, not use, not even compile) just for the course in the university of Kashan by Ahmad Yoosofan
*/

#ifndef LIST_COMPLEX_CLS_H
#define LIST_COMPLEX_CLS_H 1
#include <stdio.h>
#include "complexCls.h"
typedef struct Node_complexClsX{
	complexCls d;
	struct Node_complexClsX * next;
} Node_complexCls;
void insert_first(Node_complexCls*, complexCls);
void print_list_complexCls(Node_complexCls h);
#endif
