/*!
 * \author Ahmad Yoosofan <yoosofan@mail.com> <yoosofan@myfastmail.com>
 * \version 0.0.1
 * \date 2018
 * \copyright View only (not copy, not use, not even compile) just for the course in the university of Kashan by Ahmad Yoosofan
*/

#include "list_complexCls.h"
#include <stdlib.h>
#include <stdio.h>
void insert_first(Node_complexCls*h, complexCls a){
	Node_complexCls*p1;
	p1=(Node_complexCls*)malloc(sizeof(Node_complexCls));
	if(!p1){printf("Cannot allocate memory\n"); exit(0);}
	p1->d=a; p1->next=0;
	p1->next=h->next;
	h->next=p1;
}
void print_list_complexCls(Node_complexCls h){
	Node_complexCls*p1=h.next;
	for(;p1;p1=p1->next)
		print_complexCls(p1->d);
}
	
