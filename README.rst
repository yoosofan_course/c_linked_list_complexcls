این مخزن فقط جنبهٔ آموزشی برای کلاس درس مبانی برنامه‌نویسی احمد یوسفان در دانشگاه کاشان نیمسال پاییز ۱۳۹۷ دارد و به هیچ روی اجازهٔ به کارگیری در جای دیگری ندارد.

سپاسگزارم. احمد یوسفان

This is a simple linked list for fundamental of programming language course in the university of Kashan.

Ahmad Yoosofan

It will be remove in a month.

`c_linked_list_complexcls`

https://gitlab.com/yoosofan_course/c_linked_list_complexcls

.. code:: sh

    g++ *.c



Command line instructions
======================================

Git global setup
----------------------

.. code:: sh

    git config --global user.name "Ahmad Yoosofan"
    git config --global user.email "yoosofan@fastmail.fm"

Create a new repository
-------------------------

.. code:: sh

    git clone git@gitlab.com:yoosofan_course/c_linked_list_complexcls.git
    cd c_linked_list_complexcls
    touch README.md
    git add README.md
    git commit -m "add README"
    git push -u origin master

Existing folder
-----------------
.. code:: sh

    cd existing_folder
    git init
    git remote add origin git@gitlab.com:yoosofan_course/c_linked_list_complexcls.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master

Existing Git repository
-----------------------
.. code:: sh

    cd existing_repo
    git remote rename origin old-origin
    git remote add origin git@gitlab.com:yoosofan_course/c_linked_list_complexcls.git
    git push -u origin --all
    git push -u origin --tags

