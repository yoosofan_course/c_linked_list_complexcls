/*!
 * \author Ahmad Yoosofan <yoosofan@mail.com> <yoosofan@myfastmail.com>
 * \version 0.0.1
 * \date 2018
 * \copyright View only (not copy, not use, not even compile) just for the course in the university of Kashan by Ahmad Yoosofan
*/

#ifndef COMPLEX_CLS_H
#define COMPLEX_CLS_H 1
typedef struct{double re,img;} complexCls;
void print_complexCls(complexCls);
complexCls input_complexCls(void);
#endif
